//
//  DataBaseClass.m
//  HW_02
//
//  Created by Vitaliy Heras on 12/6/16.
//  Copyright © 2016 Vitaliy Heras. All rights reserved.
//

#import "DataBaseClass.h"
#import "Lovers+CoreDataClass.h"
#import "MyAnimals+CoreDataClass.h"




@implementation DataBaseClass


#pragma mark - Core Data stack

@synthesize persistentContainer = _persistentContainer;

- (NSPersistentContainer *)persistentContainer {
    // The persistent container for the application. This implementation creates and returns a container, having loaded the store for the application to it.
    @synchronized (self) {
        if (_persistentContainer == nil) {
            _persistentContainer = [[NSPersistentContainer alloc] initWithName:@"HW_02"];
            [_persistentContainer loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription *storeDescription, NSError *error) {
                if (error != nil) {
                    // Replace this implementation with code to handle the error appropriately.
                    // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    
                    /*
                     Typical reasons for an error here include: 
                     * The parent directory does not exist, cannot be created, or disallows writing.
                     * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                     * The device is out of space.
                     * The store could not be migrated to the current model version.
                     Check the error message to determine what the actual problem was.
                     */
                    NSLog(@"Unresolved error %@, %@", error, error.userInfo);
                    abort();
                }
            }];
        }
    }
    
    return _persistentContainer;
}

#pragma mark - Core Data Saving support

- (void)saveToDataBase: (addDataClass*) dataToSave {
    
    NSLog(@"Will store %@ %@", dataToSave.name,dataToSave.animalType);
    
    
//    NSManagedObjectContext *context = self.persistentContainer.viewContext;
//    NSError *error = nil;
//    if ([context hasChanges] && ![context save:&error]) {
//        // Replace this implementation with code to handle the error appropriately.
//        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
//        NSLog(@"Unresolved error %@, %@", error, error.userInfo);
//        abort();
//    }
//    
//    
//    NSManagedObject *customAnimal = [NSEntityDescription insertNewObjectForEntityForName:@"Lovers" inManagedObjectContext:context];
//    [customAnimal setValue:[dataToSave objectForKey:@"animalType"] forKey:@"type"];
//    [customAnimal setValue:[dataToSave objectForKey:@"name"] forKey:@"name"];
//    [customAnimal setValue:[dataToSave objectForKey:@"ownColor"] forKey:@"ownColor"];
//    [customAnimal setValue:[dataToSave objectForKey:@"age"] forKey:@"age"];
//    
//    [context save:nil];
    
}





#pragma mark - addDataDelegate

-(void) addData: (addDataClass*) addData {
    
    

    NSLog(@"Now in Data base class. Will store data : \n name: -- %@  \n ownColor: -- %@  \n type : %@ \n age : -- %d ",addData.name,addData.ownColor,addData.animalType,addData.age);

    
        NSManagedObjectContext *context = self.persistentContainer.viewContext;
        NSError *error = nil;
        if ([context hasChanges] && ![context save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, error.userInfo);
            abort();
        }
    
    
        NSManagedObject *customAnimal = [NSEntityDescription insertNewObjectForEntityForName:@"MyAnimals" inManagedObjectContext:context];
        [customAnimal setValue:[NSString stringWithFormat:@"%@",addData.animalType] forKey:@"type"];
        [customAnimal setValue:[NSString stringWithFormat:@"%@",addData.name] forKey:@"name"];
        [customAnimal setValue:[NSString stringWithFormat:@"%@",addData.ownColor] forKey:@"ownColor"];
        [customAnimal setValue:[NSNumber numberWithInteger:addData.age] forKey:@"age"];
        [customAnimal setValue:addData.photo forKey:@"photo"];
        [customAnimal setValue:addData.animalVoice forKey:@"animalVoice"];
    
        NSError* saveError = nil;
    
    
        if (![context save:&saveError]) {
            NSLog(@"--- ERROR save to DB %@",saveError);
        }
    
    NSLog(@"request getData after added data");
    [self getData];
    
    
}


- (NSArray* ) getData {

    NSLog(@"Get data from DB");
    
    
    NSManagedObjectContext *context = self.persistentContainer.viewContext;
    NSError *error = nil;
    if ([context hasChanges] && ![context save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }

    
    NSArray* resultArray =  [context executeFetchRequest:[MyAnimals fetchRequest] error:nil];

    
    
    for (MyAnimals* obj in resultArray) {
        NSLog(@"It`s a %@ with voice path - %@", obj.name, obj.animalVoice);
    }
    
    
    return resultArray;
}

@end

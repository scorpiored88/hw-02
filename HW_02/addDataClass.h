//
//  addDataClass.h
//  HW_02
//
//  Created by Vitaliy Heras on 12/7/16.
//  Copyright © 2016 Vitaliy Heras. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol addDataDelegate;

@interface addDataClass : NSObject

@property (strong,nonatomic) NSString* name;
@property (strong,nonatomic) NSString* ownColor;
@property (strong,nonatomic) NSString* animalType;
@property (assign,nonatomic) int32_t age;
@property (strong, nonatomic) NSData* photo;
@property (strong, nonatomic) NSString* animalVoice;
@property (weak,nonatomic) id <addDataDelegate> delgate;


-(void) pushData;

@end

@protocol addDataDelegate <NSObject>

-(void) addData: (addDataClass*) addData;

@end



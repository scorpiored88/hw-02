//
//  AddNewAnimalViewController.h
//  HW_02
//
//  Created by Vitaliy Heras on 12/6/16.
//  Copyright © 2016 Vitaliy Heras. All rights reserved.
//

#import "ViewController.h"
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>


@interface AddNewAnimalViewController : ViewController <UIPickerViewDataSource,UIPickerViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@property (weak, nonatomic) IBOutlet UITextField *customAnimalName;
@property (weak, nonatomic) IBOutlet UITextField *customAnimalColor;
@property (weak, nonatomic) IBOutlet UITextField *customAnimalAge;
@property (weak, nonatomic) IBOutlet UIPickerView *customAnimalType;

@property (weak, nonatomic) IBOutlet UIImageView *customAnimalImg;
@property (weak, nonatomic) IBOutlet UIButton *RecordStopBtn;



- (IBAction)dismissKeybord:(id)sender; // Щоб додати інші просто справа потягнути на функцію



- (IBAction)CameraBtn:(id)sender;


- (IBAction)LibraryBtn:(id)sender;

- (IBAction)startRecordVoice:(UIButton *)sender;
- (IBAction)stopRecording:(UIButton *)sender;

- (IBAction)playBtn:(UIButton *)sender;


@property (weak, nonatomic) IBOutlet UIButton *SaveBtn;


@end

//
//  AddNewAnimalViewController.m
//  HW_02
//
//  Created by Vitaliy Heras on 12/6/16.
//  Copyright © 2016 Vitaliy Heras. All rights reserved.
//

#import "AddNewAnimalViewController.h"
#import "DataBaseClass.h"
#import "addDataClass.h"


@interface AddNewAnimalViewController () {
    NSArray* animalsTypes;
    NSString* currentType;
    
    NSArray* UrlPath;
    
    AVAudioRecorder* recorder;
    AVPlayer* player;
    
}


@end

@implementation AddNewAnimalViewController





- (void)viewDidLoad {
    [super viewDidLoad];
    
    _customAnimalType.delegate = self;
    _customAnimalType.dataSource = self;
    
    animalsTypes = [NSArray arrayWithObjects:@"Cat",@"Cow",@"Lions",@"Rabbit",nil];
    
//    NSInteger selectedRow = [animalsTypes indexOfObject:@0];
//    
//    [_customAnimalType selectRow:selectedRow inComponent:0 animated:YES];
    
//    [_customAnimalType selectedRowInComponent:0];
    
    
//    _customAnimalType.accessibilityValue
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Medai buttons


- (IBAction)CameraBtn:(id)sender {
    UIImagePickerController* imgController = [[UIImagePickerController alloc]init];
    imgController.delegate = self;
    imgController.sourceType = UIImagePickerControllerSourceTypeCamera;
    imgController.allowsEditing = YES;
    
    [self presentViewController:imgController animated:YES completion:nil];
    
}


- (IBAction)LibraryBtn:(id)sender {
    
    UIImagePickerController* imgController = [[UIImagePickerController alloc]init];
    imgController.delegate = self;
    imgController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    imgController.allowsEditing = YES;
    
    [self presentViewController:imgController animated:YES completion:nil];
    
}


#pragma mark - Record Audio

-(void) RecordFunction {
    NSString* name = [NSString stringWithFormat:@"%@.m4a",self.customAnimalName.text];
    
    UrlPath = [NSArray arrayWithObjects:[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject],name ,nil];
    
    NSLog(@"--- path %@ -- %@", name,UrlPath);
    
    NSURL* url = [NSURL fileURLWithPathComponents:UrlPath];
    
    AVAudioSession* session = [AVAudioSession sharedInstance];
    
    [session setCategory:AVAudioSessionCategoryRecord error:nil];
    
    NSMutableDictionary* setting = [[NSMutableDictionary alloc]init];
    
    [setting setValue:[NSNumber numberWithInteger:kAudioFormatMPEG4AAC] forKey:AVFormatIDKey];
    [setting setValue:[NSNumber numberWithFloat:44100.0] forKey:AVSampleRateKey];
    [setting setValue:[NSNumber numberWithInteger:1] forKey:AVNumberOfChannelsKey];
    
    recorder = [[AVAudioRecorder alloc] initWithURL:url settings:setting error:nil];
    recorder.meteringEnabled = YES;
    
    [recorder prepareToRecord];
}

- (IBAction)startRecordVoice:(UIButton *)sender {
    
    if (![recorder isRecording]) {
        [sender setTitle:@"Stop record" forState:UIControlStateNormal];
//        _RecordStopBtn.titleLabel = @"Recording...";
        [self RecordFunction];
        [recorder record];
    } else {
        [sender setTitle:@"Rec. Voice" forState:UIControlStateNormal];
        [recorder stop];
        AVAudioSession* session = [AVAudioSession sharedInstance];
        [session setActive:NO error:nil];
    }
    

    
    
}

- (IBAction)stopRecording:(UIButton *)sender {
    
    NSLog(@"Stop Recording");
   
    [recorder stop];
    AVAudioSession* session = [AVAudioSession sharedInstance];
    [session setActive:NO error:nil];
    
    
    
}

- (IBAction)playBtn:(UIButton *)sender {
    

    NSString* correctUrl = [[NSString stringWithFormat:@"%@", recorder.url] componentsSeparatedByString:@"://"][1];
    NSLog(@"Playing!!! %@",correctUrl);
    NSLog(@" --- Playing!!! %@",[NSString stringWithContentsOfURL:recorder.url encoding:NSUnicodeStringEncoding error:nil]);
    
    if (!recorder.recording) {
        player = [[AVPlayer alloc]initWithURL:recorder.url];
//        player.volume = 2.0;
        [player play];
        
    }
}


- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage* image = [info valueForKey:UIImagePickerControllerOriginalImage];
    
//    NSData* dataImg = UIImageJPEGRepresentation([info objectForKey:UIImagePickerControllerOriginalImage], 1);
//    UIImage* image = [[UIImage alloc]initWithData:dataImg ];
    _customAnimalImg.image = image;
    
    [self dismissViewControllerAnimated:YES completion:nil];

    
}


#pragma mark - Picker

-(NSInteger) numberOfComponentsInPickerView:(UIPickerView *)pickerView {

    return 1;
}

-(NSInteger) pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [animalsTypes count];
}

-(CGFloat) pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component {
    return 30.0;
}

-(NSString*) pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [animalsTypes objectAtIndex:row];
}

- (void) pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    currentType = [NSString stringWithFormat:@"%@",[animalsTypes objectAtIndex:row]];
    NSLog(@"---- PICKER selected %@",currentType);
}


#pragma mark - SaveButtonAction & CancelButton 

- (IBAction)SaveCustomAnimal:(UIButton *)sender {
    addDataClass* customAnimal = [[addDataClass alloc]init];
    DataBaseClass* dataToDataBase = [[DataBaseClass alloc]init];
    
    NSData *imageData = UIImagePNGRepresentation(self.customAnimalImg.image);
    
    customAnimal.animalVoice = [NSString stringWithFormat:@"%@", recorder.url]; //componentsSeparatedByString:@"://"][1];;
    
//    customAnimal.voice = [NSURL fileURLWithPathComponents:UrlPath];
    customAnimal.photo = imageData;
    customAnimal.name = [NSString stringWithFormat:@"%@",self.customAnimalName.text];
    customAnimal.ownColor = [NSString stringWithFormat:@"%@",self.customAnimalColor.text];
    customAnimal.animalType = [NSString stringWithFormat:@"%@",currentType];
    
    customAnimal.age = [NSNumber numberWithInt:[self.customAnimalAge.text intValue]];
    
    
    
    customAnimal.delgate = dataToDataBase;
    
    [customAnimal pushData];
    
    
    
    [self dismissViewControllerAnimated:YES completion:NULL];
//    [self dismissModalViewControllerAnimated:YES];

}

- (IBAction)CancelBtn:(UIBarButtonItem *)sender {
    [self dismissViewControllerAnimated:YES completion:NULL];
}





/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)dismissKeybord:(id)sender {
    [self resignFirstResponder];
}


@end

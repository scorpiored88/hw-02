//
//  AnimalsTableTableViewController.m
//  HW_02
//
//  Created by Vitaliy Heras on 12/6/16.
//  Copyright © 2016 Vitaliy Heras. All rights reserved.
//

#import "AnimalsTableTableViewController.h"
#import "addDataClass.h"
#import "DataBaseClass.h"
#import "MyAnimals+CoreDataClass.h"
//#import "Lovers+CoreDataClass.h"

@interface AnimalsTableTableViewController () {
    NSMutableArray* tmpArray;
}

@property (nonatomic, strong) NSMutableArray* animalsArray;

@end

@implementation AnimalsTableTableViewController


- (void) FetchData {
    DataBaseClass* dbAnimals = [[DataBaseClass alloc]init];
    
    _animalsArray = [[NSMutableArray alloc] initWithArray:[dbAnimals getData]];
    
}


//- (void)viewWillAppear:(BOOL)animated {
//    [self FetchData];
//    [self.tableView reloadData];
//}
//
//-(void) viewDidAppear:(BOOL)animated{
//    [self FetchData];
//    [self.tableView reloadData];
//    [super viewDidAppear:animated];
//    
//    tmpArray = [NSMutableArray arrayWithObjects:@"Bill",@"John",@"Sam", nil];
//    DataBaseClass* dbAnimals = [[DataBaseClass alloc]init];
//    
//    NSLog(@"request getData and change animals array");
//    _animalsArray = [[dbAnimals getData] mutableCopy];
//    
//    for (MyAnimals* animal in _animalsArray ) {
//        NSLog(@"Here is animal: name - %@ ownColor - %@ type - %@",animal.name, animal.ownColor,animal.type);
//
//    }
//    
//    NSLog(@"--- Disapir tmp length %lu",(unsigned long)[tmpArray count]);
//    NSLog(@" --- Disapir array length %lu",(unsigned long)[_animalsArray count]);
//    
//    [self.tableView reloadData];
    
  // }

- (void)viewDidLoad {
    [super viewDidLoad];
    [self FetchData];
    
    
//    DataBaseClass* dbAnimals = [[DataBaseClass alloc]init];
//    
//    NSLog(@"request getData and change animals array");
//    _animalsArray = [[dbAnimals getData] mutableCopy];
//
//    NSLog(@" --- DidLoad tmp length %lu",(unsigned long)[tmpArray count]);
//    NSLog(@" --- DidLoad array length %lu",(unsigned long)[_animalsArray count]);
//
//    
//    for (id obj in tmpArray) {
//        NSLog(@"did load name %@",obj);
//    }
    
    
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
#warning Incomplete implementation, return the number of sections
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#warning Incomplete implementation, return the number of rows
    return [_animalsArray count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath { /// ?????????????? Можливо тут оновлювати дані
    
    
    
    static NSString* Cellid = @"Cell";
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:Cellid];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:Cellid];
        NSLog(@"nill cell");
    }

    
    
//    NSLog(@"in cell tmp length %lu",(unsigned long)[tmpArray count]);
//    NSLog(@" in cell --- array length %lu",(unsigned long)[_animalsArray count]);

    
//    NSLog(@" in cell --- array is %@",_animalsArray);
    MyAnimals* animal = [_animalsArray objectAtIndex:indexPath.row];
    NSLog(@"### name - %@ %@", animal.name, animal.type);

    
    cell.textLabel.text = [[_animalsArray objectAtIndex:indexPath.row] name];
     cell.textLabel.text = [NSString stringWithFormat:@"%@",animal.name];
    UIImage* animalImg = [[_animalsArray objectAtIndex:indexPath.row] photo] != NULL ? [UIImage imageWithData:[[_animalsArray objectAtIndex:indexPath.row] photo]] : [UIImage imageNamed:@"camera.png"];
    cell.imageView.image = animalImg;
    cell.detailTextLabel.text =  [NSString stringWithFormat:@"type: %@; ownColor: %@; age: %d" ,animal.type, animal.ownColor, animal.age];
    
//    
//    NSLog(@"animal name - %@",[animal valueForKey:@"name"]);
//    
//    cell.textLabel.text = [NSString stringWithFormat:@"%@",[animal valueForKey:@"name"]];
    
    //old
//    UIImage* allAnimalsImg = [UIImage imageWithData:[[animalsArray objectAtIndex:indexPath.row] photo]];
//    cell.imageView.image = allAnimalsImg;
//    
//    cell.textLabel.text = [[animalsArray objectAtIndex: indexPath.row]name];
//    
//    NSString* deatail = [NSString stringWithFormat:@"name : %@ ",[[animalsArray objectAtIndex:indexPath.row] name]];
//    
//    cell.detailTextLabel.text =  deatail;
    return cell;
}


- (void) initTableCell {
    

}
         

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Navigation

////In a storyboard-based application, you will often want to do a little preparation before navigation
//- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
//     Get the new view controller using [segue destinationViewController].
//     Pass the selected object to the new view controller.
//}

@end

//
//  ViewController.m
//  HW_02
//
//  Created by Vitaliy Heras on 12/6/16.
//  Copyright © 2016 Vitaliy Heras. All rights reserved.
//

#import "ViewController.h"
#import "AnimalClass.h"
#import "DataBaseClass.h"
#import "MyAnimals+CoreDataClass.h"


@interface ViewController () {
    NSInteger _counter;
    
    NSMutableArray* animalsList;
    AVPlayer* player;
    
    
}

@property (nonatomic, strong) NSArray* firstAnimalsNameArray;

@end


@implementation ViewController

@synthesize nameLabel, ageLabel, colorLabel;

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}








- (void)viewDidLoad {
    
    [self FetchData];
    
    
    nameLabel.text = [[animalsList objectAtIndex:0] name];
    //    ageLabel.text = [NSString stringWithFormat:@"%@",[[animalsList firstObject] age]];
    colorLabel.text = [NSString stringWithFormat:@"%@",[[animalsList firstObject] ownColor]];
    _aminalImg.image = [UIImage imageWithData:[[animalsList firstObject] photo]];
    
    _counter = 0;
    
    // Do any additional setup after loading the view, typically from a nib.
    [super viewDidLoad];
    
    
}





- (void) showAnimal :(BOOL) isNext {
    
    [self FetchData];                  ///// ??????????????????????
    
    
    if (isNext) {
        _counter++;
        if (_counter == [animalsList count]) {
            _counter = 0;
        }
        nameLabel.text = [[animalsList objectAtIndex:_counter] name];
//        ageLabel.text = [NSString stringWithFormat:@"%@",[[animalsList objectAtIndex:_counter] age]];
        colorLabel.text = [[animalsList objectAtIndex:_counter] ownColor];
        _aminalImg.image = [UIImage imageWithData:[[animalsList objectAtIndex:_counter] photo]];
        
    } else {
        if (_counter == 0) {
            _counter = [animalsList count];
        }
        nameLabel.text = [[animalsList objectAtIndex:(_counter - 1)] name];
//        ageLabel.text = [NSString stringWithFormat:@"%@",[[animalsList objectAtIndex:(_counter -1)] age]];
        colorLabel.text = [[animalsList objectAtIndex:(_counter -1)] ownColor];
        _aminalImg.image = [UIImage imageWithData:[[animalsList objectAtIndex:(_counter - 1)] photo]];
        --_counter;
        
    }
    
    
}


- (IBAction)nextBtn:(UIButton *)sender {
    AudioServicesPlaySystemSound(1104);
    [self showAnimal:YES];
    
}


- (IBAction)backBtn:(UIButton *)sender {
    AudioServicesPlaySystemSound(1103);
    [self showAnimal:NO];
}




- (void) FetchData {
    DataBaseClass* dbAnimals = [[DataBaseClass alloc]init];
    
    animalsList = [[NSMutableArray alloc] initWithArray:[dbAnimals getData]];

    
}





- (IBAction)sayYourName:(UIButton *)sender {
    
    [self voice:_counter];
    
}



-(void) voice :(NSInteger) currentAnimal {
    
    
    [self FetchData];
    NSString *name = [NSString stringWithFormat:@"%@",[[animalsList objectAtIndex:currentAnimal]name]];
    

    
    if ([[animalsList objectAtIndex:currentAnimal]animalVoice] != NULL) {
        
        NSString* url;
        
        if (![[[animalsList objectAtIndex:currentAnimal]animalVoice] containsString:@"file://"]){
            url =[NSString stringWithFormat:@"file://%@",[[animalsList objectAtIndex:currentAnimal]animalVoice]];
        }else {
            url = [[animalsList objectAtIndex:currentAnimal]animalVoice];
        }
        
        NSURL* urlPath = [[NSURL alloc]initWithString:url];
        
        
        NSLog(@"%@ path - %@ \n %@  ", name,urlPath,url);
        
        

        
        player = [[AVPlayer alloc]initWithURL:urlPath];
//        player.volume = 5.0;
        [player play];
        
//        SystemSoundID SoundID;
//        NSString* pathToVoice = [[NSBundle mainBundle] pathForResource:name ofType:@"m4a"];
//        NSLog(@"---- path is %@",pathToVoice);
//        AudioServicesCreateSystemSoundID((__bridge CFURLRef) [NSURL fileURLWithPath:pathToVoice], &SoundID);
//        AudioServicesPlaySystemSound(SoundID);
    }else {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:[NSString stringWithFormat:@"Hi! it`s %@",name] message:@"sorry but I can`t speek" preferredStyle:UIAlertControllerStyleAlert];
        
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alertController addAction:ok];
            [self presentViewController:alertController animated:YES completion:nil];
    }



    
    
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

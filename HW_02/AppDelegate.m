//
//  AppDelegate.m
//  HW_02
//
//  Created by Vitaliy Heras on 12/6/16.
//  Copyright © 2016 Vitaliy Heras. All rights reserved.
//

#import "AppDelegate.h"
#import "Lovers+CoreDataClass.h"
#import "DataBaseClass.h"
#import "addDataClass.h"
#import "MyAnimals+CoreDataClass.h"


@interface AppDelegate ()


@end

@implementation AppDelegate


- (NSArray*) initializeAnimalsArray {
    
   
    addDataClass* cat  = [[addDataClass alloc] init];
    cat.name = @"kitten";
//    cat.type = NSStringFromSelector(@selector(self.cat:nn));
    cat.animalType = @"Cat";
    cat.age = 3;
    cat.ownColor = @"gray";
    cat.photo = UIImagePNGRepresentation([UIImage imageNamed:@"kitten.png"]);
    cat.animalVoice = [[NSBundle mainBundle] pathForResource:@"kitten" ofType:@"m4a"];

    
    NSLog(@"voice path %@",cat.animalVoice);
    
    
    addDataClass* lion  = [[addDataClass alloc] init];
    lion.name = @"Lio";
    lion.animalType = @"Lion";
    lion.age = 8;
    lion.ownColor = @"yelow";
    lion.photo = UIImagePNGRepresentation([UIImage imageNamed:@"Lio.png"]);
    lion.animalVoice = [[NSBundle mainBundle] pathForResource:@"Lio" ofType:@"m4a"];
    
    NSLog(@"voice path %@",lion.animalVoice);
    
    addDataClass* dog  = [[addDataClass alloc] init];
    dog.name = @"Rex";
    dog.animalType = @"Dog";
    dog.age = 1;
    dog.ownColor = @"black";
    dog.photo = UIImagePNGRepresentation([UIImage imageNamed:@"Rex.png"]);
    dog.animalVoice = [[NSBundle mainBundle] pathForResource:@"Rex" ofType:@"m4a"];
    
    //NSString from class
    
    
    addDataClass* cow  = [[addDataClass alloc] init];
    cow.name = @"Milka";
    cow.animalType = @"Cow";
    cow.age = 4;
    cow.ownColor = @"Black&White";
    cow.photo = UIImagePNGRepresentation([UIImage imageNamed:@"Milka.png"]);
    cow.animalVoice = [[NSBundle mainBundle] pathForResource:@"Milka" ofType:@"m4a"];
    
    addDataClass* rabbit  = [[addDataClass alloc] init];
    rabbit.name = @"banny";
    rabbit.animalType = @"Rabbit";
    rabbit.age = 3;
    rabbit.ownColor = @"white";
    rabbit.photo = UIImagePNGRepresentation([UIImage imageNamed:@"banny.png"]);
    cat.animalVoice = [[NSBundle mainBundle] pathForResource:@"kitten" ofType:@"m4a"];
    
    NSArray* defaultAnimals = [NSArray arrayWithObjects:cat,lion,dog,cow,rabbit, nil];
    
    return defaultAnimals;
    
//    return [NSArray arrayWithObjects:cat,lion,dog,cow,rabbit, nil]; ??????????????
    
  
    
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    

//    [self saveContext];
    
    DataBaseClass* dbAnimals = [[DataBaseClass alloc]init];
    
    NSArray* animalsList = [[NSMutableArray alloc] initWithArray:[dbAnimals getData]];
    
    NSLog(@"quontity is %lu", (unsigned long)[animalsList count]);
    

    
//    NSArray* defaultAnimalsName =  @[@"Kitten",@"Lio",@"Rex",@"Milka",@"Banny"];
    

    
    
    if ([animalsList count] < 1) {
        
        NSArray* defaultAnimals = [[NSArray alloc] initWithArray:[self initializeAnimalsArray]];
        
        for (addDataClass* animal in defaultAnimals) {
            NSLog(@" %@ will be add to DB",animal.name);
            [dbAnimals addData:animal];
        }
        
    }
    


    
    
    
//    NSManagedObjectContext* context = [self managedObjectContext];
//    NSManagedObject *customAnimal = [NSEntityDescription insertNewObjectForEntityForName:@"Animals" inManagedObjectContext:context];
//    [customAnimal setValue:@"Cat" forKey:@"type"];
//    [customAnimal setValue:@"Hupopo" forKey:@"name"];
//    [customAnimal setValue:@"Red" forKey:@"ownColor"];
//    [customAnimal setValue:@12 forKey:@"age"];
//    NSError *error = nil;
//    if (![context save:&error]) {
//        NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
//    }
//
//    
//    NSLog(@"Get data from DB");
//    
//    NSMutableArray* animalsArray;
//    
//    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
//    
//    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Animals"];
//    
//    
//    
//    animalsArray = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
//    
//    NSLog(@"array is %@", animalsArray);
    


    



    
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    
    
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}


#pragma mark - Core Data stack

@synthesize persistentContainer = _persistentContainer;

- (NSPersistentContainer *)persistentContainer {
    // The persistent container for the application. This implementation creates and returns a container, having loaded the store for the application to it.
    @synchronized (self) {
        if (_persistentContainer == nil) {
            _persistentContainer = [[NSPersistentContainer alloc] initWithName:@"HW_02"];
            [_persistentContainer loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription *storeDescription, NSError *error) {
                if (error != nil) {
                    // Replace this implementation with code to handle the error appropriately.
                    // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    
                    /*
                     Typical reasons for an error here include:
                     * The parent directory does not exist, cannot be created, or disallows writing.
                     * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                     * The device is out of space.
                     * The store could not be migrated to the current model version.
                     Check the error message to determine what the actual problem was.
                    */
                    NSLog(@"Unresolved error %@, %@", error, error.userInfo);
                    abort();
                }
            }];
        }
    }
    
    return _persistentContainer;
}

#pragma mark - Core Data Saving supp




- (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;

}


- (void)saveContext {
    NSManagedObjectContext *context = self.persistentContainer.viewContext;
    NSError *error = nil;
    if ([context hasChanges] && ![context save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
//    
//       NSManagedObject *customAnimal = [NSEntityDescription insertNewObjectForEntityForName:@"MyAnimals" inManagedObjectContext:context];
//        [customAnimal setValue:@"Cow" forKey:@"type"];
//        [customAnimal setValue:@"Burka" forKey:@"name"];
//        [customAnimal setValue:@"Black" forKey:@"ownColor"];
//       
//
//        [context save:nil];
    
    

    
    
        NSLog(@"Get data from DB AppDelegate");
    


    
//        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Animals"];
//        NSArray* resultArray =  [context executeFetchRequest:fetchRequest error:nil];
    
    NSArray* resultArray =  [context executeFetchRequest:[MyAnimals fetchRequest] error:nil]; //в класі вже метод fetchRequest
    
    
    
    for (MyAnimals* obj in resultArray) {
        NSLog(@"It`s a %@ with %@ ownColor  and type - %@", obj.name, obj.ownColor,obj.type);
    }
    
//        NSLog(@"array is %@", resultArray);
    
    
}





//- (NSArray)

@end

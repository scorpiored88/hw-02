//
//  ViewController.h
//  HW_02
//
//  Created by Vitaliy Heras on 12/6/16.
//  Copyright © 2016 Vitaliy Heras. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>

//#import "AnimalClass.h"


@interface ViewController : UIViewController


@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *ageLabel;
@property (weak, nonatomic) IBOutlet UILabel *colorLabel;
@property (weak, nonatomic) IBOutlet UIImageView *aminalImg;



- (IBAction)sayYourName:(UIButton *)sender;

@end

